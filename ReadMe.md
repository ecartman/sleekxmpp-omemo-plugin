# Introduction
This is an experimental plugin with only basic features for now.

*Note that every commit should be signed.*

It works with Gajim and Conversations.

# TODO:
 - [x] User can setup via config if he want to autotrust keys
 - [x] Handle multiple device ids
 - [x] Remove the old signed prekey after amount of time
 - [ ] Check if our device is in pep after my other device was added
 - [x] Fix support for Gajim-Omemo
 - [x] Support for Conversations Messaging
 - [ ] Add support for Groupchat messaging
 - [x] Add support for sending single user chat messaging
 - [ ] Better exception handling and code cleaning
 - [ ] Better Documentation
 - [x] Show Fingerprints to user
 - [ ] Change trust level
 - [x] Get all own devices and save them
 - [x] Check the key amount and create new ones if needed
 - [ ] Check if <payload> tag in message or is KeyTransport
 - [ ] File transfer (there seems to be an already finished plugin for that)
