NS_EME = 'urn:xmpp:eme:0'
NS_OMEMO = 'eu.siacs.conversations.axolotl'
NS_DEVICELIST = NS_OMEMO + '.devicelist'
NS_NOTIFY = NS_DEVICELIST + '+notify'
NS_BUNDLES = NS_OMEMO + '.bundles:'

# We want to enable Debug Mode ?
DEBUG_FLAG = False


# Sqlite3 Database file name
DB_FILE = "example.db"
AUTOTRUST = True
